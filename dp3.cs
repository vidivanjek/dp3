using System; //TODO: using Gtk;

static class Verzija{
  /*
    Dohvaca verziju iz file-a "ver", ili vraca zapeceni podatak ako nije moguc
    pristup do file-a.
    
  */
  public static string ver;
  static Verzija(){
     try{
       var sr=new System.IO.StreamReader("ver");
       ver=sr.ReadToEnd();
       sr.Close();   
       sr.Dispose();
#pragma warning disable 168
     }catch (Exception nebitno){
#pragma warning restore 168
//Pragma iskljucuje upozorenje prevoditelja da se "nebitno" ne koristi.
       ver="3.0.4";
     }
  }
}//Kraj clase Version.

/*TODO: class Prozor : Window {

  public Prozor():base("Dp3 v."+Verzija.ver+" "){
    Application.Init();
//    this.Title="Dp3 v."+Verzija.ver;
    this.Resize(400,300);
    Label l=new Label();l.Text="Hellp wprld!";this.Add(l);
    this.ShowAll();
    Application.Run();
  }
}*/

static class CitacRecepta {
  /*
    Metode ovog razreda (klase) omogucuju citanje recepta.
    Recept je u pravilu file koji opisuje jednu ili vise kutija.
    Po potrebi se stvaraju novi objekti te se pozivaju njihovi karakteristicni
    citaci parametara. Na kraju definicije svakog dijela kutije se izraduju
    nacrti tog dijela kutije.
  */
  public static void Napravi(string receptFName){
    /*
      Glavna metoda ovog razreda. Otvara file recepta i obavlja potrebne
      radnje za pretvaranje recepta u nacrte ovisno o opisanim dijelovima kutije
      iz recepta.
    */
    //rr=recept reader
    var rr=new System.IO.StreamReader(receptFName);
    //tr=trenutni recept
    string tr=rr.ReadToEnd();
    rr.Close();
    rr.Dispose();
    
    int i=0;//Prati do kuda smo dosli s citanjem recepta.
            //Na pocetku krecemo od pocetka (0 bajtova smo prosli).
                    

       
    var zadnjaKadica=new Kadica();
    
    /* U pravilu svaka kutija krece od kadice. Ova varijabla prati zadnju
       kadicu i prema njoj radi dijelove kutije koji se dodaju na kadicu.
       To su poklopac, korica i slicno.
       
       Ako netko zeli biti haker i koristiti ovaj program za nacrtati
       koricu knjige za tvrdi uvez neka koristi slijedeci trik:
       treba definiradi kadicu s debljinom ljepenke 0,
       sa sirinom koja odgovara mjeri knjiznog bloka od vrha do dna,
       sa duzinom koja odgovara mjeri knjiznog bloka od hrpta do suprotnog kraja,
       sa visinom koja odgovara debljini knjiznog bloka.
       Onda treba definirati koricu s tri elementa, svakako navesti debljinu
       ljepenke za koricu. Srednji element za hrbat se moze i ne mora fizicki
       ugraditi, nacrt je prakticti isti. */
       
    while(i<tr.Length){//Dokle god imamo jos recepta radimo.
      string vrsta=Rijec(tr, ref i); //Citamo rijec po rijec.
      if(vrsta=="kadica"){//Ako je kadica:
        Kadica novaKadica=new Kadica();//napravi novi objekt,
        zadnjaKadica=novaKadica;//zapamti da je ovo najnovija kadica prema
        //kojoj cemo mozda trebati napraviti poklopac ili koricu ili nesto,
        ProdjiParametre(novaKadica, tr, ref i);//pa procitaj parametre (kadice).
      }else if(vrsta=="poklopac"){//A ako je poklopac...
        Poklopac noviPoklopac=new Poklopac(zadnjaKadica);
        ProdjiParametre(noviPoklopac, tr, ref i);
      }else if(vrsta=="korica-s-magnetom"){//Ako je korica s magnetom...
        MagnetskaKorica novaMagnetskaKorica=new MagnetskaKorica(zadnjaKadica);
        ProdjiParametre(novaMagnetskaKorica, tr, ref i);
      }else if(vrsta=="korica"){
        Korica novaKorica=new Korica(zadnjaKadica);
        ProdjiParametre(novaKorica, tr, ref i);
      }else if(vrsta=="suber"){
        Suber noviSuber=new Suber();
        ProdjiParametre(noviSuber, tr, ref i);
      }
    }//Kraj while(i<=len)-a.
    //Prosli smo cijeli recept.
  }//Kraj Napravi()-ja.
  
  static void ProdjiParametre(Nacrt nacrt, string tr, ref int i){
    /*
      Kad smo u receptu pronasli novi dio kutije, treba procitati i njegove
      parametre. Svaki dio kutije zavrsava s "kraj"-em.
    */
    while(true){//Rucno cemo prekinuti ako trebamo.
      if(i>=tr.Length){break;}//Nema smisla citati dalje od kraja pa u tom
                              //slucaju prekidamo.
      //Ako nismo upravo prekinuli onda ima dalje reecpta pa citamo iducu rijec.
      string rijec=Rijec(tr, ref i);
      if(nacrt.InterpretirajRecept(tr, ref i, rijec)){
        //InterpertirajRecept() je vec napravio posao pa ne treba dalje nista.
        //InterpretirajRecept() vraca true ako je prepoznao rijec, a inace vraca
        //false.
      }else if(rijec=="kraj"){//Ako smo dosli do kraja nekog dijela kutije
        break;//prekidamo i zavrsavamo.
      }
      
    }//Kraj while(true)
    //Kad smo procitajli sve parametre nekog dijela kutije onda ga trebamo
    //nacrtati.
    nacrt.Crtaj();
  }
  
  public static void PocistiRazmake(string s, ref int i){
    /*
      Preskace (visestruke) razmake.
    */
    for(;i<s.Length;++i){
      if(Char.IsWhiteSpace(s[i])){continue;}
      else{break;}
    }
  }

  public static string Rijec(string s, ref int i){
    /*
      Rijeci su razdvojene bjelinama (razmacima). Ova metoda cita jednu rijec.
    */
    PocistiRazmake(s,ref i);
    string r="";
    for(; i<s.Length; ++i){
      if(Char.IsWhiteSpace(s[i])){break;}
      r+=Char.ToLower(s[i]);
    }
    return r;    
  }
  
  public static long Broj(string s, ref int i){
   /*
     Slicno kao Rijec(), samo cita broj. U receptima su brojevi pisani s
     decimalnim zarezom, a tocka se moze koristiti za tisucice. Program
     zanemaruje sve vanzemaljce* unutar broja. *To jest, sve znakove koje ne
     ocekuje. Npr. ako netko na kraju broja napise "mm" program se nece
     naljutiti.
     
     Brojevi su fiksne preciznosti do na mikron. Zaista za kartonske kutije
     veca preciznost nije potrebna. Ali zbog jednostavnosti (i obicaja struke)
     u osnovi su prikazani kao da su u milimetrima, a mikroni se dobivaju
     kao decimalni dio milimetra. Unutar sebe program pohranjuje brojeve
     u mikronima.
     
   */
   PocistiRazmake(s,ref i);
   long cijeli=0;long decimale=0;//cijeli i decimalni dio broja u mm
   long broj=0;//Ukupna vrijednost u mikronima.
   long dm=100;//decimalno mjesto (100, 10 i 1 mikrona)
   bool cijeliDio=true;//Prvo gledamo cijeli dio, a ako dodjemo do decimalnog
                       //zareza prelazimo na decimalni dio pa ce ovo postati
                       //false.
   for(;i<s.Length;++i){//Nastavljamo citati recept znak po znak.
     if(Char.IsWhiteSpace(s[i])){//Ako smo dosli do razmaka, to je kraj broja.
       break;
     }
     if(Char.IsDigit(s[i])){//Nova znamenka.
       if(cijeliDio){
         cijeli*=10;//Guramo sve prethodne znamenke lijevo,
         cijeli+=(long)Char.GetNumericValue(s[i]);//pa dodajemo novu znamenku.
         continue;//Rijesili smo novu znamenku pa idemo dalje.
       }
       else{//!cijeliDio, u tom slucaju citamo decimale.
         if(dm>0){//Zanimaju nas samo decimale od barem mikron vrijednosti.
           decimale+=dm*(long)Char.GetNumericValue(s[i]);//Postavlja decimalu
             //na pravo decimalno mjesto.
           dm/=10;//Ako ce biti iduce decimalno mjesto biti ce manje vrijednosti
                  //za brojevnu bazu puta.
           continue;//Rijesili smo decimalu pa idemo dalje.
         }//Kraj if(dm>0).
       }//Kraj else.
     }//Kraj if(IsDigit).
     if(s[i]==','){//Ako smo nasli decimalni zarez
       cijeliDio=false;//prelazimo s cijelog na decimalni dio broja.
       continue;//Rijesili smo decimalni zarez pa idemo dalje.
     }//Kraj if(',').
   }//Kraj for(i).
   broj=cijeli*1000+decimale;//Broj se sastoji od cijelog dijela u mm i
     //decimalnog dijela milimetra.
   return broj;
  }//Kraj Broj().

}//Kraj klase CitacRecepta.

class Dp3 { //Gavni dio programa.

  public static void Main(string[] args){
   
    //TODO:Prozor p=new Prozor();
  foreach(var receptFileName in args){
    try{//Zatvoremo je u try jer mozda file ne postoji ili tako nesto.
      //Ne zelimo srusiti ovaj program zbog nebitne sitnice.
      CitacRecepta.Napravi(receptFileName);
    }finally{
      continue;//Samo da nije prazno.
    }
  }
  
  }//Kraj Main().

}//Kraj klase Dp3.
