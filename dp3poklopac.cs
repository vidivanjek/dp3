class Poklopac:Kadica{

  public long up;//upasivanje poklopca
  public Kadica nastavakNa;//veza na osnovni element (dno/kadicu/ladicu)
  public Kadica nast => nastavakNa; //kratki oblik

  
  void upasiPoklopac(){
    x=nast.x+2*nast.lj+2*up;
    y=nast.y+2*nast.lj+2*up;
  }
  
  public Poklopac(Kadica pk,
                  long pup=1000L){
    naziv=pk.naziv+"-poklopac";
    imaLjepenka=true;imaPresvlaka=true;
    nastavakNa=pk;
    h=pk.h;
    up=pup;
    upasiPoklopac();
    lj=nast.lj;
    os=nast.os;
    op=nast.op;
    nzlj=nast.nzlj;
    brmag=0;
    m=nast.m;
    rzlj=nast.rzlj;
    
  }
  
  public override bool InterpretirajRecept(string tr, ref int i, string rijec){
    bool ret=false;
    if(i>=tr.Length){return false;}
    if(base.InterpretirajRecept(tr, ref i, rijec)){
      ret=true;
    }else if(rijec=="up" || rijec=="upasivanje-poklopca"){
      up=CitacRecepta.Broj(tr, ref i);
      upasiPoklopac();
      ret=true;
    }
    
    return ret;
  }

}
