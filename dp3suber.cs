class Suber:Nacrt{

  public long lj;//debljina ljepenke
  public long v;//visina duz hrpta
  public long d;//dubina od otvora do hrpta
  public long s;//sirina knjiznog bloka s koricama
  public long os;//okret sa strane
  long _r;//rund
  public long r{get{return _r<0?(s<30000 ? s/3 : 10000):_r;}set{_r=value;}}
  long rzlj,rzlj2;//razmak za ljepenku 1 i 2
  
  void racunajRzlj(){
    rzlj=lj<=2000 ? lj : 1000*((long)( (((double)lj)/1000.0) * System.Math.Sqrt(2.0) ));
    rzlj2=1000*(long)((((double)lj)/1000.0)*System.Math.Sqrt(5.0));
  }
  
  public Suber(long plj=2000L,
               long pv=298000L,
               long pd=211000L,
               long ps=20000L,
               long pos=20000L,
               long pr=-1L,
               long pm=5000L
  ){
    m=pm;
    lj=plj;
    racunajRzlj();
    v=pv;
    d=pd;
    s=ps;
    os=pos;
    _r=pr;
    imaLjepenka=true;imaPresvlaka=true;
  }
  
  public override void RacunajVelicinuNacrtaZaLjepenku(){
    xi=yi=m;
    sirinaNacrta=m+d+s+d+m;
    duzinaNacrta=m+s+lj+v+lj+s+m;
  }
  
  public override void RacunajVelicinuNacrtaZaPresvlaku(){
    xi=yi=m;
    sirinaNacrta=m+os+d+rzlj+s+rzlj+d+os+m;
    visinaNacrta=m+s+rzlj+lj+v+lj+rzlj+s+m;
  }
  
  public override void CrtajLjepenku(System.IO.StreamWriter sw){
    xi=yi=m;
    
    /*
    Karikirano
       
      _d__
    s[____]_ _d__
     |    |H[____]s
     |    |r|    |
v+2lj|Lice|b|Leđa|v
     |    |a|    |
     |    |t|____|
     |____|_[____]s
    s[____]s
    
      Hrbat je po duljini v+4lj
    
    */
    
    //rez uz lice
    sw.Write("<polyline class=\"rez fil0\" points=\"");
    sw.Write((m+d)+","+(m+s)+" ");
    sw.Write((m+d)+","+(m)+" ");
    sw.Write((m)+","+(m)+" ");
    sw.Write((m)+","+(m+s+lj+v+lj+s)+" ");
    sw.Write((m+d)+","+(m+s+lj+v+lj+s)+" ");
    sw.Write((m+d)+","+(m+s+lj+v+lj)+" ");
    sw.Write("\"/>");sw.WriteLine();
    
    //rez uz ledja
    sw.Write("<path class=\"rez fil0\" d=\"");
    sw.Write("M "+(m+d+s)+","+(m+lj+s)+" ");
    sw.Write("L "+(m+d+s)+","+(m+lj+r)+" ");
    sw.Write("A "+(r)+","+(r)+" 0 0 1 "+(m+d+s+r)+","+(m+lj)+" ");
    sw.Write("L "+(m+d+s+d)+","+(m+lj)+" ");
    sw.Write("L "+(m+d+s+d)+","+(m+lj+s+v+s)+" ");
    sw.Write("L "+(m+d+s+r)+","+(m+lj+s+v+s)+" ");
    sw.Write("A "+(r)+","+(r)+" 0 0 1 "+(m+d+s)+","+(m+lj+s+v+s-r)+" ");
    sw.Write("L "+(m+d+s)+","+(m+lj+s+v)+" ");
    sw.Write("\"/>");sw.WriteLine();
    
    //gornji rez uz hrbat
    sw.Write("<polyline class=\"rez fil0\" points=\"");
    sw.Write((m+d)+","+(m+s-lj)+" ");
    sw.Write((m+d+s)+","+(m+s-lj)+" ");
    sw.Write("\"/>");sw.WriteLine();

    //donji rez uz hrbat
    sw.Write("<polyline class=\"rez fil0\" points=\"");
    sw.Write((m+d)+","+(m+s+lj+v+lj+lj)+" ");
    sw.Write((m+d+s)+","+(m+s+lj+v+lj+lj)+" ");
    sw.Write("\"/>");sw.WriteLine();
    
    //ric uz lice
    sw.Write("<polyline class=\"ric fil0\" points=\"");
    sw.Write((m)+","+(m+s)+" ");
    sw.Write((m+d)+","+(m+s)+" ");
    sw.Write((m+d)+","+(m+s+lj+v+lj)+" ");
    sw.Write((m)+","+(m+s+lj+v+lj)+" ");
    sw.Write("\"/>");sw.WriteLine();    

    //ric uz ledja
    sw.Write("<polyline class=\"ric fil0\" points=\"");
    sw.Write((m+d+s+d)+","+(m+lj+s)+" ");
    sw.Write((m+d+s)+","+(m+lj+s)+" ");
    sw.Write((m+d+s)+","+(m+lj+s+v)+" ");
    sw.Write((m+d+s+d)+","+(m+lj+s+v)+" ");
    sw.Write("\"/>");sw.WriteLine();    

  }
  
  public override void CrtajPresvlaku(System.IO.StreamWriter sw){
    /*
    
    0___1    12___13
    [   )2  11/   ]
    [  3|6___7|   ]
    [   \/5 8\/10 ]
    [   4    9    ]
    [             ]
    [             ]
    [   /\___/\   ]
    [   |     |   ]
    [___)     \___]
    
    
    */
    long[] xt=new long[14];//x-koordinate tocaka
    long[] yt=new long[14];//y-koordinate tocaka
    long xpom56, ypom56;//pomocna tocka izmedju 5 i 6
    long xpom78, ypom78;//pomocna tocka izmedju 7 i 8
    //napetosti za bezierove krivulje
    System.Collections.Generic.Dictionary<long, long> xb=new System.Collections.Generic.Dictionary<long, long>();
    System.Collections.Generic.Dictionary<long, long> yb=new System.Collections.Generic.Dictionary<long, long>();
    double l456, k456;//pravac 4---pom56
    double l789, k789;//pravac pom78---9
    //Pom56 i pom78 su tocke koje bi bile umjesto 5 i 6
    //odnosno 7 i 8 kada ne bilo zaobljeno nego sam spica.
    
    long xp,yp;//x pocetak, y pocetak
    xp=m; yp=m+lj;
    xt[0]=0;//Pocetak, pomak od xp i
    yt[0]=0;//yp.
    xt[1]=xt[0]+os+d-lj-r;//prije runda
    yt[1]=yt[0];
    xt[2]=xt[0]+os+d-lj;//poslje runda
    yt[2]=yt[0]+r;
     xb.Add( 1, xt[1]+((long)((double)(r)*0.552284749831)) );
     yb.Add( 1, yt[1] );
     xb.Add( 2, xt[2] );
     yb.Add( 2, yt[2]-((long)((double)(r)*0.552284749831)) );
    xt[3]=xt[2];//kad se spustimo
    yt[3]=yt[0]+s-lj-lj-lj;
    xt[4]=xt[3]+lj+rzlj;//prva spica uz hrbat
    yt[4]=yt[3]+lj;
    xpom56=xt[4]+lj;
    ypom56=yt[4]+lj-os;if(ypom56<yt[0]){ypom56=yt[0];};
    if(lj==0){//Ne postoji ljepenka 0 debljine
              //jer bi bila 0 debljine odnosno ne bi
              //postojala ako ovaj opis besmislene situacije
              //ima smisla.
      l456=999999.999;//Pa koristimo besmisleni broj za
                     //za besmislenu situaciju.
    }else{
      l456=(double)(yt[4]*xpom56-xt[4]*ypom56)/(double)(xpom56-xt[4]);
    }
    if(xpom56==0.0){//Ovo se može dogoditi samo ako je kutija
                  //veličine 0 ili tako nesto, pa za besmislenu situaciju
      k456=999999.999;//koristimo besmisleni broj.
    }else{
      k456=((double)ypom56-l456)/(double)xpom56;
    }
    yt[6]=ypom56;
    xt[6]=xpom56+r;
    yt[5]=yt[6]+r;
    xt[5]=k456==0.0 ? xt[6] : (long)(((double)yt[5]-l456)/k456);//Opet, u
      //besmislenoj situaciji radimo besmislice, a u smislenoj
      //situaciji radimo kako treba.
     yb.Add( 5, (yt[5])-(long)((double)r*0.55) );
     xb.Add( 5, (long)(((double)(yb[5])-l456)/k456) );
     xb.Add( 6, xt[6]-(long)((double)r*0.55) );
     yb.Add( 6, yt[6] );
    
    xt[9]=xt[4]+s;
    yt[9]=yt[4];
    xpom78=xt[9]-lj;
    ypom78=ypom56;
    if(lj==0){//Ne postoji ljepenka 0 debljine
              //jer bi bila 0 debljine odnosno ne bi
              //postojala ako ovaj opis besmislene situacije
              //ima smisla.
      l789=999999.999;//Pa koristimo besmisleni broj za
                     //za besmislenu situaciju.
    }else{
      l789=(double)(yt[9]*xpom78-xt[9]*ypom78)/(double)(xpom78-xt[9]);
    }
    if(xpom78==0){//Ovo se može dogoditi samo ako je kutija
                  //veličine 0, pa za besmislenu situaciju
      k789=999999.999;//koristimo besmisleni broj.
    }else{
      k789=(double)(ypom78-l789)/(double)xpom78;
    }   sw.Write("L "+(xp+xt[2])+","+(yp+yt[2])+" ");
 
    yt[7]=ypom78;
    xt[7]=xpom78-r;
    yt[8]=yt[7]+r;
    xt[8]=k789==0.0 ? xt[7] : (long)(((double)yt[8]-l789)/k789);//Opet, u
      //besmislenoj situaciji radimo besmislice, a u smislenoj
      //situaciji radimo kako treba.
     yb.Add( 8, yb[5] );
     xb.Add( 8, (long)(((double)(yb[8])-l789)/k789) );
     xb.Add( 7, xt[7]+(long)((double)r*0.55) );
     yb.Add( 7, yt[7] );
    xt[10]=xt[9]+2*lj;
    yt[10]=yt[3];
    xt[11]=xt[10];
    yt[11]=yt[2];
    xt[12]=xt[11]+r;
    yt[12]=yt[1];
    xt[13]=os+d+rzlj+s+rzlj+d+os;
    yt[13]=yt[0];
    if(s<20000){
      yt[11]+=lj/2;
      yt[12]+=lj/2;
      yt[13]+=lj/2;
    }

    sw.Write("<path class=\"rez fil0\" d=\"");
    sw.Write("M "+(xp+xt[0])+","+(yp+yt[0])+" ");
    sw.Write("L "+(xp+xt[1])+","+(yp+yt[1])+" ");
    sw.Write("C "+(xp+xb[1])+","+(yp+yb[1])+" "+
                  (xp+xb[2])+","+(yp+yb[2])+" "+
                  (xp+xt[2])+","+(yp+yt[2])+" ");
    sw.Write("L "+(xp+xt[3])+","+(yp+yt[3])+" ");
    sw.Write("L "+(xp+xt[4])+","+(yp+yt[4])+" ");
    sw.Write("L "+(xp+xt[5])+","+(yp+yt[5])+" ");
    sw.Write("C "+(xp+xb[5])+","+(yp+yb[5])+" "+
                  (xp+xb[6])+","+(yp+yb[6])+" "+
                  (xp+xt[6])+","+(yp+yt[6])+" ");
    sw.Write("L "+(xp+xt[7])+","+(yp+yt[7])+" ");
    sw.Write("C "+(xp+xb[7])+","+(yp+yb[7])+" "+
                  (xp+xb[8])+","+(yp+yb[8])+" "+
                  (xp+xt[8])+","+(yp+yt[8])+" ");
    sw.Write("L "+(xp+xt[9])+","+(yp+yt[9])+" ");
    sw.Write("L "+(xp+xt[10])+","+(yp+yt[10])+" ");
    sw.Write("L "+(xp+xt[11])+","+(yp+yt[11])+" ");
    sw.Write("L "+(xp+xt[12])+","+(yp+yt[12])+" ");
    sw.Write("L "+(xp+xt[13])+","+(yp+yt[13])+" ");

    xp=m;
    yp=m+s+rzlj+lj+v+lj+rzlj+s-lj;
    
    sw.Write("L "+(xp+xt[13])+","+(yp-yt[13])+" ");
    sw.Write("L "+(xp+xt[12])+","+(yp-yt[12])+" ");
    sw.Write("L "+(xp+xt[11])+","+(yp-yt[11])+" ");
    sw.Write("L "+(xp+xt[10])+","+(yp-yt[10])+" ");
    sw.Write("L "+(xp+xt[9])+","+(yp-yt[9])+" ");
    sw.Write("L "+(xp+xt[8])+","+(yp-yt[8])+" ");
    sw.Write("C "+(xp+xb[8])+","+(yp-yb[8])+" "+
                  (xp+xb[7])+","+(yp-yb[7])+" "+
                  (xp+xt[7])+","+(yp-yt[7])+" ");
    sw.Write("L "+(xp+xt[6])+","+(yp-yt[6])+" ");
    sw.Write("C "+(xp+xb[6])+","+(yp-yb[6])+" "+
                  (xp+xb[5])+","+(yp-yb[5])+" "+
                  (xp+xt[5])+","+(yp-yt[5])+" ");
    sw.Write("L "+(xp+xt[4])+","+(yp-yt[4])+" ");
    sw.Write("L "+(xp+xt[3])+","+(yp-yt[3])+" ");
    sw.Write("L "+(xp+xt[2])+","+(yp-yt[2])+" ");
    sw.Write("C "+(xp+xb[2])+","+(yp-yb[2])+" "+
                  (xp+xb[1])+","+(yp-yb[1])+" "+
                  (xp+xt[1])+","+(yp-yt[1])+" ");
    sw.Write("L "+(xp+xt[0])+","+(yp-yt[0])+" ");
    sw.Write("Z\"/>");sw.WriteLine("");
    
    CrtajOblik.PravokutnikVelicine(
      sw,"pozicija",m+os,m,d,s);//iznad lica
    CrtajOblik.PravokutnikVelicine(
      sw,"pozicija",m+os,m+s+rzlj,d,lj+v+lj);//lice
    CrtajOblik.PravokutnikVelicine(
      sw,"pozicija",m+os,m+s+rzlj+lj+v+lj+rzlj,d,s);//ispod lica
    CrtajOblik.PravokutnikVelicine(
      sw,"pozicija",m+os+d+rzlj,m+s+rzlj-lj,s,v+4*lj);//hrbat
    CrtajOblik.PravokutnikVelicine(
      sw,"pozicija2",m+os+d+rzlj+s+rzlj,m+s+rzlj+lj-rzlj2-s,d,s);//iznad ledja
    CrtajOblik.PravokutnikVelicine(
      sw,"pozicija",m+os+d+rzlj+s+rzlj,m+s+rzlj+lj,d,v);//ledja
    CrtajOblik.PravokutnikVelicine(
      sw,"pozicija2",m+os+d+rzlj+s+rzlj,m+s+rzlj+lj+v+rzlj2,d,s);//ispod lica
  }
  
  public override bool InterpretirajRecept(string tr, ref int i, string rijec){
  /*
  public long lj;//debljina ljepenke
  public long v;//visina duz hrpta
  public long d;//dubina od otvora do hrpta
  public long s;//sirina knjiznog bloka s koricama
  public long os;//okret sa strane
  public long r;//rund
  long rzlj,rzlj2;//razmak za ljepenku 1 i 2
  */
    bool ret=false;
    if(i>=tr.Length){return false;}
    if(base.InterpretirajRecept(tr, ref i, rijec)){
      ret=true;
    }else if(rijec=="ljepenka" || rijec=="lj"){
      lj=CitacRecepta.Broj(tr, ref i);
      ret=true;
    }else if(rijec=="visina" || rijec=="v"){
      v=CitacRecepta.Broj(tr, ref i);
      ret=true;
    }else if(rijec=="dubina" || rijec=="d"){
      d=CitacRecepta.Broj(tr, ref i);
      ret=true;
    }else if(rijec=="sirina" || rijec=="debljina" || rijec=="s"){
      s=CitacRecepta.Broj(tr, ref i);
      ret=true;
    }else if(rijec=="okrret-sa-strane" || rijec=="os"){
      os=CitacRecepta.Broj(tr, ref i);
      ret=true;
    }else if(rijec=="rund" || rijec=="r"){
      r=CitacRecepta.Broj(tr, ref i);
      ret=true;
    }
    
    return ret;
  }

}
