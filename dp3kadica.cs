class Kadica:Nacrt{

  public long lj;//debljina materijala
  public long x;//sirina; obicno >=duzina
  public long y;//duzina; obicno <=sirina
  public long h;//visina;
  public long nzlj;//napust za ljepenku, obicno 1000
  public long rzlj;//razmak za poziciju ljepenke na nacrtu presvlake
  public long brmag;//broj magneta
  public long magpor;//pomak (sredine) magneta od ruba kutije ako su dva magneta
  public long magpod;//pomak (sredine) magneta od dna kutije
  public long magvel;//velicina magneta
  public long os;//okret sa strane za presvlaku
  public long op;//okrez preko za presvlaku
  public long zub;//zub na korici kutije na magnet ili trakicu
  
  void racunajRzlj(){
    rzlj= lj<=2000 ? lj : 1000*((long)( (((double)lj)/1000.0) * System.Math.Sqrt(2.0) ));
  }
  
  public Kadica(long px=100000L,
                long py=75000L,
                long ph=50000L,
                long plj=2000L,
                long pos=15000L,
                long pop=15000L,
                long pnzlj=1000L,
                long pbrmag=0L,
                long pmagvel=5500L,
                long pmagpor=25000L,
                long pmagpod=25000L,
                long pzub=3000L,
                long pm=5000L

  ){
   x=px;y=py;h=ph;
   lj=plj;nzlj=pnzlj;
   racunajRzlj();
   os=pos;op=pop;
   brmag=pbrmag;magvel=pmagvel;magpor=pmagpor;magpod=pmagpod;
   m=pm;
   zub=pzub;
   imaLjepenka=true;imaPresvlaka=true;
  }

  public override void RacunajVelicinuNacrtaZaLjepenku(){
    xi=yi=m;
    long xvc, yvc;
    xvc=m+h+x+h+m;
    yvc=m+h+y+h+m;
    sirinaNacrta=xvc;
    duzinaNacrta=yvc;
  }
  
  public override void RacunajVelicinuNacrtaZaPresvlaku(){
    xi=yi=m;

    long xvc,xvct,yvc;

    xvc=m+op+lj+h+rzlj+x+rzlj+h+lj+op+m;
    xvct=m+os+lj+x+lj+os+m;
    if(xvct>xvc){
      xi=m+(xvct-xvc);
      xvc=xvct;
    }
    yvc=m+op+lj+h+rzlj+y+rzlj+h+lj+op+m;
    sirinaNacrta=xvc;
    duzinaNacrta=yvc;
  }
  
  void StranaLjepenkeLD(System.IO.StreamWriter sw, long x1, long y1, long xd, long yd){
    //x1,y1=pocetak; xd,yd=delta (pomak)
    sw.WriteLine("<polyline class=\"rez fil0\" points=\""+
                 x1+","+y1+" "+
                 (x1+xd)+","+y1+" "+
                 (x1+xd)+","+(y1+yd)+" "+
                 x1+","+(y1+yd)+"\"/>");
  }
  void StranaLjepenkeGD(System.IO.StreamWriter sw, long x1, long y1, long xd, long yd){
    //x1,y1=pocetak; xd,yd=delta (pomak)
    sw.WriteLine("<polyline class=\"rez fil0\" points=\""+
                 x1+","+y1+" "+
                 x1+","+(y1+yd)+" "+
                 (x1+xd)+","+(y1+yd)+" "+
                 (x1+xd)+","+y1+"\"/>");
  }  
  public override void CrtajLjepenku(System.IO.StreamWriter sw){
    xi=yi=m;
    //TODO: provjeriti sumanutu situaciju ako je napust veci od visine
    //i prilagoditi ishodiste prema potrebi.
  /*
       ____x____
      h|   G   |h
    _h_|_______|_h_
    |  |   x   |  |
   Z|L |y  S  y| D|Z=y+2*lj*nzlj/1000
    |__|___x___|__|
     h |   Do  | h
      h|___x___|h
       
         
        
  */
    //"S" - Srednji pravokutnik (ricevi)
    CrtajOblik.Pravokutnik(sw,"ric",m+h,m+h,m+h+x,m+h+y);
    
    //"G"-prva strana bez napusta
    StranaLjepenkeGD(sw,xi+h,yi+h,x,-h);
    //"Do"-druga strana bez napusta
    StranaLjepenkeGD(sw,xi+h,yi+h+y,x,h);
    //"L"-prva strana s napustom
    StranaLjepenkeLD(sw,xi+h,yi+h-lj*nzlj/1000,-h,y+2*(lj*nzlj/1000));
    //"D"-druga strana s napustom
    StranaLjepenkeLD(sw,xi+h+x,yi+h-lj*nzlj/1000,h,y+2*(lj*nzlj/1000));
    
    //ako nema magneta (brmag==0) ne treba crtati utore za magnete
    if(brmag==1){
      //ako je jedan magnet onda ide u sredinu
      CrtajOblik.Krug(sw,"rez",xi+h+x/2,yi+h+lj-magpod,magvel);
    }else if(brmag>1){
      //ako je dva ili više magneta onda ih rasporedimo od ruba do ruba
      long magporzk=magpor-zub-lj;
      for(long i=0;i<brmag;++i){
        CrtajOblik.Krug(sw,"rez",xi+h+magporzk+(x-2*magporzk)*i/(brmag-1),
          yi+h+lj-magpod,magvel);
      }
    }
  }
  
  void StranaPresvlake(System.IO.StreamWriter sw, long x1, long y1, long xd, long yd){
      CrtajOblik.Pravokutnik(sw,"pozicija",x1,y1,x1+xd,y1+yd);
  }
  
  public override void CrtajPresvlaku(System.IO.StreamWriter sw){
    
    //prva strana bez napusta, tu su magneti ako postoje
    StranaPresvlake(sw,xi+op+lj+h+rzlj, yi+op+lj, x, h);
    //druga strana bez napusta
    StranaPresvlake(sw,xi+op+lj+h+rzlj, yi+op+lj+h+rzlj+y+rzlj, x, h);
    //prva strana s napustom
    StranaPresvlake(sw,xi+op+lj, yi+op+lj+h+rzlj-(lj*nzlj/1000), h, y+2*(lj*nzlj/1000));
    //druga strana s napustom
    StranaPresvlake(sw,xi+op+lj+h+rzlj+x+rzlj, yi+op+lj+h+rzlj-(lj*nzlj/1000), h, y+2*(lj*nzlj/1000));
    //sredina
    StranaPresvlake(sw,xi+op+lj+h+rzlj, yi+op+lj+h+rzlj, x, y);
    
    {//
      //xs/ys je x-smjer i y-smjer predznak (-1 ili +1)
      //xs=xs<0?-1:1;ys=ys<0?-1:1;
      /* Oznake cetvrtina:
          ___
        1[   ]2   1 - xs=+; ys=+; 2 - xs=-; ys=+;
        [  X  ]   3 - xs=-; ys=-; 4 - xs=+; ys=-;
        4[___]3   redosljed tocaka: 1 i 3 = 0-10: 2 i 4 = 10-0
      */
      /* Tocke unutar (prve) cetvrtine:
      
               10_____________(11)
                 |
                 |
            7___/ 9
             |  8
             |  
        0 1 6|___5 
         --\_____>4
         |  2   3
         |
         |                      -
         |(-1)              -(xc,yc)+
                                +
                
      */
      //x-tocke i y-tocke
      long[] xt=new long[11];
      long[] yt=new long[11];
      long xc=xi+op+lj+h+rzlj+x/2;//x centar
      long yc=yi+op+lj+h+rzlj+y/2;//y centar 

      xt[0]=-x/2-rzlj-h-lj-op;
      yt[0]=-y/2-lj/2;
      
      xt[1]=xt[0]+op;
      yt[1]=yt[0];

      xt[2]=xt[1]+lj;
      yt[2]=yt[1]+lj/2;

      xt[3]=xt[2]+h-lj/2;
      yt[3]=yt[2];

      xt[4]=xt[2]+h;
      yt[4]=yt[0];

      xt[5]=xt[4]-((h<20000)?4000:5000);
      yt[5]=yt[4]-((h<20000)?4000:5000);
      
      xt[6]=-x/2-lj-os;
      yt[6]=yt[5];
      
      xt[7]=xt[6];
      yt[7]=-y/2-rzlj-h+lj/2;
      
      xt[8]=-x/2-3*lj;
      yt[8]=yt[7];
      
      xt[9]=-x/2-lj/2;
      yt[9]=-y/2-rzlj-h-2*lj;
      
      xt[10]=xt[9];
      yt[10]=-y/2-rzlj-h-lj-op;
      
      
      sw.Write("<polygon class=\"rez fil0\" points=\"");
      
      //prva crtvrtina
      for(int i=0;i<xt.Length;++i){
        sw.Write((xc+xt[i])+","+(yc+yt[i])+" ");
      }
      //druga cetvrtina
      for(int i=xt.Length-1;i>=0;--i){
        sw.Write((xc-xt[i])+","+(yc+yt[i])+" ");
      }
      //treca crtvrtina
      for(int i=0;i<xt.Length;++i){
        sw.Write((xc-xt[i])+","+(yc-yt[i])+" ");
      }
      //cetvrta cetvrtina
      for(int i=xt.Length-1;i>=0;--i){
        sw.Write((xc+xt[i])+","+(yc-yt[i])+" ");
      }
      
      sw.Write("\"/>");sw.WriteLine("");
      
    }
      //TODO:magneti
  }
  public override bool InterpretirajRecept(string tr, ref int i, string rijec){
  /*  long lj;//debljina materijala
  long x;//sirina; obicno >=duzina
  long y;//duzina; obicno <=sirina
  long h;//visina;
  long nzlj;//napust za ljepenku, obicno 1000
  long rzlj;
  long brmag;//broj magneta
  long magpor;//pomak (sredine) magneta od ruba kutije ako su dva magneta
  long magpod;//pomak (sredine) magneta od dna kutije
  long magvel;//velicina magneta
  long m;//margina na rubu crteza
  long os;//okret sa strane za presvlaku
  long op;//okrez preko za presvlaku
  long zub;//zub na korici kutije na magnet
  */
  bool ret=false;//did we find something of interest?
        if(i>=tr.Length){return false;}
        if(base.InterpretirajRecept(tr, ref i, rijec)){
          ret=true;
        }else if(rijec=="ljepenka" || rijec=="lj"){
          lj=CitacRecepta.Broj(tr,ref i); 
          ret=true; 
        }else if(rijec=="sirina" || rijec=="x"){
          x=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="duzina" || rijec=="y"){
          y=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="visina" || rijec=="v" || rijec=="h" || rijec=="dubina"){
          h=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="ljepenka-napust" || rijec=="nzlj" || rijec=="napust-za-ljepenku"){
          nzlj=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="brmag" || rijec=="broj-magneta"){
          brmag=CitacRecepta.Broj(tr,ref i)/1000;
          ret=true;
        }else if(rijec=="magpor" || rijec=="magnet-pomak-od-ruba"){
          magpor=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="magpod" || rijec=="magent-pomak-od-dna"){
          magpod=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="magvel" || rijec=="magnet-velicina" || rijec=="velicina-magneta"){
          magvel=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="os" || rijec=="okret-sa-strane"){
          os=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="op" || rijec=="okret-preko"){
          op=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }else if(rijec=="zub" || rijec=="korice-zub" || rijec=="zub-za-korice"){
          zub=CitacRecepta.Broj(tr,ref i);
          ret=true;
        }
        
        return ret;
  }
}
  
