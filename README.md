Dno-poklopac 3

This is the third re-work of dno-poklopac. Dno-poklopac literally means "bottom-lid", which was what the first installment was meant to draw.

This program takes in parameters (such as size and material properties) of a rigid cardboard box and makes die-cut plans for it.

Dno-pokopac 3 is still a work in progress. However, basic functionality currently works without known problems.

To compile using mono, use command like:
$ mcs -out:dp3.exe dp*.cs

To run program make a file like provided sample dp3recept.txt containing box specifications. Then, either drag-and-drop txt file into exe or run exe from terminal with txt as a parameter like:
$ dp3.exe dp3recept.txt

Copyright (C) 2018-2021 Vid Ivanjek. All rights reserved. At least until program is finished. Once it is finished I will probably make a more permissive license for it.

Quick changelog:

3.0.0 - base vesrion, works for bottom+lid type boxes.

3.0.1 - adds magetbox support

3.0.2 - adds support for non-magnetic cover (closing by satin lace or gravity)

3.0.4 - adds support for book sleeve