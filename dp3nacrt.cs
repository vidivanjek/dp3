class Nacrt{

  //Sve mjere su u mikronima.
  
  public string naziv;
  
  public long m;//margina na rubu crteza
  
  public bool imaLjepenka;//ovaj dio kutije ima/nema nacrt za ljepenku
  public bool imaPresvlaka;//ima/nema nacrt za presvlaku
  
  public long sirinaNacrta; public long duzinaNacrta;
  public long visinaNacrta {get{return duzinaNacrta;}set{duzinaNacrta=value;}}
  public long xi; public long yi;//ishodiste nacrta
  
  public virtual void RacunajVelicinuNacrtaZaLjepenku(){
    sirinaNacrta=50000L;duzinaNacrta=50000L;
  }
  
  public virtual void RacunajVelicinuNacrtaZaPresvlaku(){
    sirinaNacrta=50000L;duzinaNacrta=50000L;
  }

  public virtual void CrtajLjepenku(System.IO.StreamWriter sw){    
  }
  
  public virtual void CrtajPresvlaku(System.IO.StreamWriter sw){
  }
  
  public Nacrt(){
    naziv="moja_kutija";
    sirinaNacrta=50000L;duzinaNacrta=50000L;//5x5 cm samo da compiler ne zanovijeta
    xi=yi=m=5000L;
    imaLjepenka=false;imaPresvlaka=false;
  }
    
  void Zaglavlje(System.IO.StreamWriter sw,long w,long h){
  sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
  sw.WriteLine("<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">");
  sw.WriteLine(" ");
  sw.WriteLine("<svg xmlns=\"http://www.w3.org/2000/svg\" xml:space=\"preserve\" width=\""+(w/1000)+"."+(w%1000)+"mm\" height=\""+(h/1000)+"."+(h%1000)+"mm\" version=\"1.1\" style=\"shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd\"");
  sw.WriteLine("viewBox=\"0 0 "+w+" "+h+"\"");
  sw.WriteLine(" xmlns:xlink=\"http://www.w3.org/1999/xlink\">");
  sw.WriteLine("<defs>");
  sw.WriteLine("<style type=\"text/css\">");
  sw.WriteLine("  <![CDATA[");
  sw.WriteLine("    .ric {stroke: rgb(0,170,170) ; stroke-width:76.2 }");
  sw.WriteLine("    .rez {stroke: rgb(255,0,0) ; stroke-width:76.2 }");
  sw.WriteLine("     .pozicija {stroke: rgb(0,0,0) ; stroke-width:76.2 }");
  sw.WriteLine("     .pozicija2 {stroke: rgb(255,255,0) ; stroke-width:76.2 }");
  sw.WriteLine("    .fil0 {fill:none;fill-rule:nonzero}");
  sw.WriteLine("   ]]>");
  sw.WriteLine("  </style>");
  sw.WriteLine(" </defs>");
  sw.WriteLine(" <g id=\"nacrt\">");
  sw.WriteLine("");
  sw.WriteLine("");
  }
  
  void Podnozje(System.IO.StreamWriter sw){
   sw.WriteLine(" </g>");
   sw.WriteLine("</svg>");   
  }
  
  public void Crtaj(){
    bool[] truefalse=new bool[]{true,false};
    foreach(var a in truefalse){//dvije skoro iste stvari za redom
      if(a?imaLjepenka:imaPresvlaka){
        string fname= naziv+"-"+
                      (a?"ljepenka":"presvlaka")+
                      ".svg";
        var sw=
          new System.IO.StreamWriter(fname,false,/*System.Text.UTF8Encoding*/System.Text.Encoding.UTF8);
        if(a){RacunajVelicinuNacrtaZaLjepenku();}else{RacunajVelicinuNacrtaZaPresvlaku();}
        Zaglavlje(sw,sirinaNacrta,visinaNacrta);
        if(a){CrtajLjepenku(sw);}else{CrtajPresvlaku(sw);}
        Podnozje(sw);
        sw.Flush();
        sw.Close();
        sw.Dispose();
      }
    }
  }
  
  public virtual bool InterpretirajRecept(string tr, ref int i, string rijec){
      bool ret=false;
      if(i>=tr.Length){return false;}
      if(rijec=="//"){
           for(;i<tr.Length;++i){
             if(tr[i]=='\r' || tr[i]=='\n'){
              CitacRecepta.PocistiRazmake(tr, ref i);
              ret=true;
              break;
             }
           }
      }else if(rijec=="naziv"){
          naziv=CitacRecepta.Rijec(tr,ref i);
          ret=true;
      }else if(rijec=="m" || rijec=="margina"){
          m=CitacRecepta.Broj(tr,ref i);
          ret=true;
      }else if(rijec=="/*"){
           for(string r=CitacRecepta.Rijec(tr,ref i);i<tr.Length;r=CitacRecepta.Rijec(tr, ref i)){
             if(r=="*/"){ret=true;break;}
           }
           ret=true;
     }
     return ret;
  }
  
  static public long Hodaj(long koliko, ref long gdje){
    gdje+=koliko; return koliko;
  }
  
}

static class CrtajOblik{
    public static void Pravokutnik(System.IO.StreamWriter sw, string style,
                            long x1, long y1, long x2, long y2){
      sw.WriteLine("<polygon class=\""+style+" fil0\" points=\""+
                   x1+","+y1+" "+
                   x1+","+y2+" "+
                   x2+","+y2+" "+
                   x2+","+y1+"\"/>");
    }
    
    public static void PravokutnikVelicine(System.IO.StreamWriter sw, string style,
                            long x1, long y1, long dx, long dy){
      Pravokutnik(sw, style, x1,y1,x1+dx, y1+dy);
    }
                            
    public static void Krug(System.IO.StreamWriter sw, string style,
                     long cx, long cy, long r){
      sw.WriteLine("<circle class=\""+style+" fil0\" "+
                   "cx=\""+cx+"\" "+
                   "cy=\""+cy+"\" "+
                   "r=\""+r+"\" "+
                   " />");
    }

    public static void PresvlakaKorice(System.IO.StreamWriter sw, string style,
                                       long xi,long yi,long dx,long dy,long ch){
/*
   1_____________8
  2/             \7
   | [] [...] [] |
  3\_____________/6
   4             5
*/
      sw.WriteLine("<polygon class=\""+style+" fil0\" points=\""+
        (xi+ch)+","+(yi)+" "+//1
        (xi)+","+(yi+ch)+" "+//2
        (xi)+","+(yi+dy-ch)+" "+//3
        (xi+ch)+","+(yi+dy)+" "+//4
        (xi+dx-ch)+","+(yi+dy)+" "+//5
        (xi+dx)+","+(yi+dy-ch)+" "+//6
        (xi+dx)+","+(yi+ch)+" "+//7
        (xi+dx-ch)+","+(yi)+" "+//8
        "\"/>");
    }

}

