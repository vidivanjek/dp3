class MagnetskaKorica:Nacrt{

  public Kadica nastavakNa; //veza na osnovni element (dno/kadicu/ladicu)
  public Kadica nast => nastavakNa; //kratki oblik
  
  long ibrel;//internal representation
  public long brel{get{return ibrel;}set{ibrel=value<3?3:(value>5?5:value);}}
  //broj elemenata mora biti (3,4, ili 5)
  public long lj;//debljina ljepenke korice
  public long op;//okret preko
  public long upe;//upasivanje 5. elementa
  public long brmag;//broj magneta
  
  public MagnetskaKorica(Kadica pk, long pbrel=5, long pbrmag=-1, long plj=-1,
                         long pop=20000, long pupe=1000, long pm=5000){
    if(plj<0){lj=pk.lj;}
    if(pbrmag<0){brmag=pk.brmag;}else{brmag=pbrmag;}
    m=pm;
    imaLjepenka=false;
    imaPresvlaka=true;
    brel=pbrel;
    nastavakNa=pk;
    naziv=pk.naziv+"-magnetska_korica";
    op=pop;
    upe=pupe;
  }
  
  public override void RacunajVelicinuNacrtaZaPresvlaku(){
    xi=yi=m;
    duzinaNacrta=2*m+2*(op+nast.zub+nast.lj)+nast.x;
    sirinaNacrta=2*m+2*op+nast.h+nast.lj+lj*2+nast.y+nast.lj*2+lj*2+nast.h+nast.lj;
    if(brel>=4){sirinaNacrta+=nast.y+nast.lj*2+lj*2;}
    if(brel>4){sirinaNacrta+=nast.h+nast.lj+lj*3+upe;}
    
  }
  
  void PravokutnikSMagnetima(System.IO.StreamWriter sw, string style,
                             long x1, long y1, long dx, long dy){
    CrtajOblik.PravokutnikVelicine(sw,style,
                           x1,y1,dx,dy);
    if(nast.brmag==1){
      CrtajOblik.Krug(sw,style,x1+dx-nast.magpod, y1+dy/2, nast.magvel);
    }else if(brmag>1){
      for(long i=0;i<brmag;++i){
        CrtajOblik.Krug(sw,style,
          x1+dx-nast.magpod,
          y1+nast.magpor+(dy-2*nast.magpor)*i/(brmag-1),
          nast.magvel);
      }
    }
  }
  
  public override void CrtajPresvlaku(System.IO.StreamWriter sw){
    long px=0;//px=pomak po x smjeru
    long py=yi+op;//py=pocetak u y smjeru
    long dy=nast.x+2*(nast.lj+nast.zub);//dy=visina svih pravokutnika
    if(brel==3){
    
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             Hodaj(xi+op,ref px),
                             py,
                             Hodaj(nast.h+nast.lj, ref px),
                             dy);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.y+2*nast.lj, ref px),
                             dy);
      PravokutnikSMagnetima(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.h+nast.lj, ref px),
                             dy);
    
    }else if(brel==4){
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             Hodaj(xi+op,ref px),
                             py,
                             Hodaj(nast.y+2*nast.lj, ref px),
                             dy);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.h+nast.lj, ref px),
                             dy);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.y+2*nast.lj, ref px),
                             dy);
      PravokutnikSMagnetima(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.h+nast.lj, ref px),
                             dy);

    }else{//brel==5
      PravokutnikSMagnetima(sw,"pozicija",
                             Hodaj(xi+op,ref px),
                             py,
                             Hodaj(nast.h+nast.lj, ref px),
                             dy);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.y+2*nast.lj, ref px),
                             dy);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.h+nast.lj, ref px),
                             dy);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
                             px+Hodaj(2*lj,ref px),
                             py,
                             Hodaj(nast.y+2*nast.lj, ref px),
                             dy);
      PravokutnikSMagnetima(sw,"pozicija",
                             px+Hodaj(3*lj+upe,ref px),
                             py,
                             Hodaj(nast.h+nast.lj, ref px),
                             dy);
    }
    CrtajOblik.PresvlakaKorice(sw,"rez",xi,yi,sirinaNacrta-2*m,visinaNacrta-2*m,
      (long)(2.0*(double)op-2.0*((double)lj*(double)1.0606601717798212)));
      /* Apropos abrakadabraka geometrije...
      
      ..CH._____________
      .   /D
      CH /
      . /  X _____________
      ./     |A
      /B     |
      |      |
      |      |
      |      |
      
      
      Razmak X od ugla A pod 45 stupnjeva do sredine duzine BD mora biti
      1,5 x debljina ljepenke korice. Duzina BD mora biti pod 45 stupnjeva.
      
      Udaljenost CH po X (i isto tako po Y) osi od produzetka ravnih linija
      je kateta jednakokracnog pravokutnog trokuta i ono cudoviste:
      
      (long)(2.*(double)op-2.*((double)lj*(double)1.0606601717798212))
      
      je zapravo formula koja racuna taj CH.
      
      */

  }
  
  public override bool InterpretirajRecept(string tr, ref int i, string rijec){
    /*
      public long brel{}//broj elemenata
      public long lj;//debljina ljepenke korice
      public long op;//okret preko
      public long upe;//upasivanje 5. elementa
    */
    bool ret=false;
    if(i>tr.Length){return false;}
    if(base.InterpretirajRecept(tr,ref i, rijec)){
      ret=true;
    }else if(rijec=="brel" || rijec=="broj-elemenata"){
      brel=CitacRecepta.Broj(tr,ref i)/1000;
      ret=true;
    }else if(rijec=="lj" || rijec=="ljepenka"){
      lj=CitacRecepta.Broj(tr,ref i);
      ret=true;
    }else if(rijec=="op" || rijec=="okret-preko"){
      op=CitacRecepta.Broj(tr,ref i);
      ret=true;
    }else if(rijec=="upe" || rijec=="upasivanje-5.-elementa" || rijec=="upasivanje-5tog-elementa"){
      upe=CitacRecepta.Broj(tr,ref i);
      ret=true;
    }else if(rijec=="brmag" || rijec=="broj-magneta"){
          brmag=CitacRecepta.Broj(tr,ref i);
          if(brmag<0){brmag=nast.brmag;}else{brmag/=1000;}
          ret=true;
    }
    return ret;
  }


}

class Korica:Nacrt{

  public Kadica nastavakNa; //veza na osnovni element (dno/kadicu/ladicu)
  public Kadica nast => nastavakNa; //kratki oblik
  
  long ibrel;//internal representation
  public long brel{get{return ibrel;}set{ibrel=3;}}
  //planiram podrsku i za druge vrijednosti brel-a (od 2 do 5) ali o tom  po  tom
  public long lj;//debljina ljepenke korice
  public long op;//okret preko
  public long upe;//upasivanje 5. elementa
  
  public Korica(Kadica pk, long pbrel=5, long plj=-1,
                         long pop=20000, long pupe=1000, long pm=5000){
    if(plj<0){lj=pk.lj;}

    m=pm;
    imaLjepenka=false;
    imaPresvlaka=true;
    brel=pbrel;
    nastavakNa=pk;
    naziv=pk.naziv+"-korica";
    op=pop;
    upe=pupe;
  }
  
  public override void RacunajVelicinuNacrtaZaPresvlaku(){
    xi=yi=m;
    duzinaNacrta=2*m+2*(op+nast.zub+nast.lj)+nast.x;
    sirinaNacrta=2*m+2*op+2*(nast.y+2*nast.lj+nast.zub)+nast.h+nast.lj+lj*2*2;
   
  }
  
  public override void CrtajPresvlaku(System.IO.StreamWriter sw){
    long px=0;//px=pomak po x smjeru
    long py=yi+op;//py=pocetak u y smjeru
    long dy=nast.x+2*(nast.lj+nast.zub);//dy=visina svih pravokutnika
    if(brel==3){
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
        Hodaj(xi+op,ref  px),
        py,
        Hodaj(nast.y+nast.lj*2+nast.zub, ref px),
        dy);
      Hodaj(2*lj,ref  px);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
        px,
        py,
        Hodaj(nast.h+nast.lj, ref px),
        dy);
      Hodaj(2*lj,ref  px);
      CrtajOblik.PravokutnikVelicine(sw,"pozicija",
        px,
        py,
        Hodaj(nast.y+nast.lj*2+nast.zub, ref px),
        dy);

    
    }else if(brel==4){
      //TODO
    }else{//brel==5
      //TODO
    }
    CrtajOblik.PresvlakaKorice(sw,"rez",xi,yi,sirinaNacrta-2*m,visinaNacrta-2*m,
      (long)(2.0*(double)op-2.0*((double)lj*(double)1.0606601717798212)));
      /* Apropos abrakadabraka geometrije...
      
      ..CH._____________
      .   /D
      CH /
      . /  X _____________
      ./     |A
      /B     |
      |      |
      |      |
      |      |
      
      
      Razmak X od ugla A pod 45 stupnjeva do sredine duzine BD mora biti
      1,5 x debljina ljepenke korice. Duzina BD mora biti pod 45 stupnjeva.
      
      Udaljenost CH po X (i isto tako po Y) osi od produzetka ravnih linija
      je kateta jednakokracnog pravokutnog trokuta i ono cudoviste:
      
      (long)(2.*(double)op-2.*((double)lj*(double)1.0606601717798212))
      
      je zapravo formula koja racuna taj CH.
      
      */

  }
  
  public override bool InterpretirajRecept(string tr, ref int i, string rijec){
    /*
      public long brel{}//broj elemenata
      public long lj;//debljina ljepenke korice
      public long op;//okret preko
      public long upe;//upasivanje 5. elementa
    */
    bool ret=false;
    if(i>tr.Length){return false;}
    if(base.InterpretirajRecept(tr,ref i, rijec)){
      ret=true;
    }else if(rijec=="brel" || rijec=="broj-elemenata"){
      brel=CitacRecepta.Broj(tr,ref i)/1000;
      ret=true;
    }else if(rijec=="lj" || rijec=="ljepenka"){
      lj=CitacRecepta.Broj(tr,ref i);
      ret=true;
    }else if(rijec=="op" || rijec=="okret-preko"){
      op=CitacRecepta.Broj(tr,ref i);
      ret=true;
    }else if(rijec=="upe" || rijec=="upasivanje-5.-elementa" || rijec=="upasivanje-5tog-elementa"){
      upe=CitacRecepta.Broj(tr,ref i);
      ret=true;
    }
    return ret;
  }


}

